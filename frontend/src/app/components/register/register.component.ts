import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {   AbstractControl, FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';

import { IDropdownSettings } from 'ng-multiselect-dropdown';


import Validation from './CustomValidators';
import { Options, LabelType } from '@angular-slider/ngx-slider';
// import { PasswordStrengthValidator } from "./password-strength.validators"
// import {ThemePalette} from '@angular/material/core';
@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

	/* dateArr = [{ name: 'carlo', date: '2012-01-23'}, { name: 'carla', date: '1999-10-20'}];

	changedDate(e :any) {
		const value = this.filterDate(new Date(e));

		if (value) {
		console.log('Object found', value)
		}
		else {
		console.log('Nothing found')
		}
	}

	filterDate(selectedDate: Date) {
		for(const d of this.dateArr) {
			const elemDate = new Date(d.date);
			if (elemDate.getDate() === selectedDate.getDate()) {
				return d;
			}
		}
	} */

	minValue: number = 50;
	maxValue: number = 400;
	options: Options = {
		floor: 0,
		ceil: 500,
		translate: (value: number, label: LabelType): string => {
		switch (label) {
			case LabelType.Low:
			return "<b>Min price:</b> ₹" + value;
			case LabelType.High:
			return "<b>Max price:</b> ₹" + value;
			default:
			return "₹" + value;
		}
		}
	};

	// model: NgbDateStruct;
	form: FormGroup = new FormGroup({
		fullname: new FormControl(''),
		// username: new FormControl(''),
		email: new FormControl(''),
		password: new FormControl(''),
		confirmPassword: new FormControl(''),
		acceptTerms: new FormControl(false),
	});

	submitted = false;

	/* Hobbies: Array<any> = [
		{ name: 'cricket', value: 'cricket'},
		{ name: 'tv', value: 'tv'},
	]; */
	orders :any[]= [];

	constructor(private formBuilder: FormBuilder, http: HttpClient) {
		this.form = this.formBuilder.group({
			cricket: true,
			tv: true,
			// orders: ['']
		});
		this.orders = this.getOrders();

	}
	getOrders() {
		return [
		{ "id": '1', "name": 'order 1' },
		{ "id": '2', "name": 'order 2' },
		{ "id": '3', "name": 'order 3' },
		{ "id": '4', "name": 'order 4' }
		];
	}

	dropdownList :any[]= [];
	dropdownSettings:IDropdownSettings={};

	ngOnInit(): void {
		this.form = this.formBuilder.group(
			{
				fullname: ['', Validators.required],

				mobile: [
					'',
					[
						Validators.required,
						Validators.pattern("^[0-9]*$"),
						Validators.minLength(10), Validators.maxLength(10)

					]
				],

				url: [
					'',
					[
						Validators.required,
						Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')
					]
				],
				/* username: [
					'',
					[
						Validators.required,
						Validators.minLength(6),
						Validators.maxLength(20)
					]
				], */

				file: new FormControl('', [Validators.required]),
    			fileSource: new FormControl('', [Validators.required]),
				email: ['', [Validators.required, Validators.email]],
				password: [
					'',
					[
						Validators.required,
						/* Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)([a-zA-Z0-9!@#$%^&*]{6,16})$'), */
						Validators.pattern('(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])$'),
						Validators.minLength(6),
						Validators.maxLength(40),
					]
				],
				confirmPassword: ['', Validators.required],
				acceptTerms: [false, Validators.requiredTrue]
			},
			{
				validators: [Validation.match('password', 'confirmPassword')]
			},

		);

		this.dropdownList =[
			{ "item_id": 1 , "item_text": "pinal" },
			{ "item_id": 2 , "item_text": "test" },
		];
		this.dropdownSettings = {
			idField: 'item_id',
			textField: 'item_text',

		};


	}

	get f(): { [key: string]: AbstractControl } {
		return this.form.controls;

	}

	onFileChange(event:any) {

		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.form.patchValue({
			fileSource: file
			});
		}
	}
	onSubmit(): void {

		const formData = new FormData();
    	formData.append('file', this.form.get('fileSource')?.value);
		this.submitted = true;
		if (this.form.invalid) {
			return;
		}

		console.log(JSON.stringify(this.form.value, null, 2));

	}
	onReset(): void {
		this.submitted = false;
		this.form.reset();
	}
}
